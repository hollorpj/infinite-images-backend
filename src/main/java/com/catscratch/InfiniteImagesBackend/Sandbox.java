package com.catscratch.InfiniteImagesBackend;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;

public class Sandbox {
	
	public static BufferedImage generateRandomBWImage(int width, int height) {
		
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				int red = (int) (Math.random() * 255);
				int green = (int) (Math.random() * 255);
				int blue = (int) (Math.random() * 255);
				int randomColor = new Color(red, green, blue).getRGB();
				
				img.setRGB(j, i, randomColor);
			}
		}
		
		return img;
	}
	
	public static byte[] convertImageToBase64(BufferedImage img) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(img, "png", baos);
		return Base64.getEncoder().encode(baos.toByteArray());
	}

	public static void main(String[] args) throws IOException {
		BufferedImage br = ImageIO.read(new File("sample.jpg"));
	
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(br, "jpg", baos);
		byte[] bytes = baos.toByteArray();
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		
		long start = System.currentTimeMillis();
		BufferedImage cpy = generateRandomBWImage(256, 256);
		System.out.println(System.currentTimeMillis() - start);
		
		byte[] base64 = convertImageToBase64(cpy);
		ByteArrayInputStream b64stream = new ByteArrayInputStream(Base64.getDecoder().decode(base64));
		BufferedImage out = ImageIO.read(b64stream);
		
		ImageIO.write(out, "png", new File("un.png"));
		
	}
	
}
