package com.catscratch.InfiniteImagesBackend;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Router {

	@CrossOrigin("http://localhost:4200")
	@RequestMapping(value="/imageData", method=RequestMethod.GET)
	public ResponseEntity<byte[]> fetchRandomImage() throws IOException {
		BufferedImage randomImage = generateRandomColorImage(256, 256);
		byte[] output = convertImageToBase64(randomImage);
		
		return new ResponseEntity<byte[]>(output, HttpStatus.OK);
	}
	
	@CrossOrigin("http://localhost:4200")
	@GetMapping(value = "/random-image", produces = MediaType.IMAGE_JPEG_VALUE)
	public @ResponseBody byte[] getRandomImage() throws IOException {
	    BufferedImage img = generateRandomColorImage(256, 256);
	    ByteArrayOutputStream bos = new ByteArrayOutputStream();
	    ImageIO.write(img, "jpg", bos);
	    return bos.toByteArray();
	}
	
	@CrossOrigin("http://localhost:4200")
	@GetMapping(value = "/random-image/{width}/{height}", produces = MediaType.IMAGE_JPEG_VALUE)
	public @ResponseBody byte[] getRandomImage(@PathVariable int width, @PathVariable int height) throws IOException {
	    BufferedImage img = generateRandomColorImage(width, height);
	    ByteArrayOutputStream bos = new ByteArrayOutputStream();
	    ImageIO.write(img, "jpg", bos);
	    return bos.toByteArray();
	}
	
	@RequestMapping(value = "/givemeimages/{number}/{width}/{height}", method=RequestMethod.GET)
	public void giveMeImages(HttpServletResponse response, @PathVariable int number, @PathVariable int width, @PathVariable int height) throws IOException {	
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();		
		ZipOutputStream zout = new ZipOutputStream(bos);
		
		for (int i = 0; i < number; i++) {
			addRandomImageToZip(zout, width, height, String.valueOf(i) + ".jpg");	
		}
		
		closeZipStream(zout);
		
		response.addHeader("Content-disposition", "attachment;filename=images.zip");
		response.setContentType("application/zip");

		ByteArrayInputStream zipBis = new ByteArrayInputStream(bos.toByteArray());
		IOUtils.copy(zipBis, response.getOutputStream());		
		response.flushBuffer();		
	}
	
	private void closeZipStream(ZipOutputStream zout) throws IOException {
		zout.finish();
		zout.flush();
		zout.close();
	}

	private void addRandomImageToZip(ZipOutputStream zout, int width, int height, String name) throws IOException {
		ZipEntry z = new ZipEntry(name);
		zout.putNextEntry(z);
		
		BufferedImage toAdd = generateRandomColorImage(width, height);		
		ByteArrayOutputStream imageByteArray = new ByteArrayOutputStream();
		ImageIO.write(toAdd, "jpg", imageByteArray);
		ByteArrayInputStream bis = new ByteArrayInputStream(imageByteArray.toByteArray());
		
		IOUtils.copy(bis, zout);
		
		bis.close();
		imageByteArray.close();
		
		zout.closeEntry();
	}

	public static BufferedImage generateRandomColorImage(int width, int height) {
		
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				int red = (int) (Math.random() * 255);
				int green = (int) (Math.random() * 255);
				int blue = (int) (Math.random() * 255);
				int randomColor = new Color(red, green, blue).getRGB();
				
				img.setRGB(j, i, randomColor);
			}
		}
		
		return img;
	}
	
	public static BufferedImage generateRandomGrayscaleImage(int width, int height) {
		
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				int red = (int) (Math.random() * 255);
				int green = (int) (Math.random() * 255);
				int blue = (int) (Math.random() * 255);
				int randomColor = new Color(red, green, blue).getRGB();
				
				img.setRGB(j, i, randomColor);
			}
		}
		
		return img;
	}
	
	public static byte[] convertImageToBase64(BufferedImage img) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(img, "png", baos);
		return Base64.getEncoder().encode(baos.toByteArray());
	}
	

}
