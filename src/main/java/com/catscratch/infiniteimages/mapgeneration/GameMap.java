package com.catscratch.infiniteimages.mapgeneration;

import java.util.LinkedHashMap;
import java.util.Map;

import com.catscratch.infiniteimages.model.Coordinate;

public class GameMap {

	private int width;
	private int height;
	private Map<Coordinate, String> map;
	
	private final Coordinate startCoordinate;
	private final Coordinate endCoordinate;
	
	public GameMap(int width, int height) {
		this.width = width;
		this.height = height;
		this.map = new LinkedHashMap<Coordinate, String>();
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				Coordinate pos = new Coordinate(j, i);
				String value = "-1";
				map.put(pos, value);
			}
		}
		
		int startX = (int) (Math.random() * width);
		int startY = (int) (Math.random() * height);
		this.startCoordinate = new Coordinate(startX, startY);
		
		int endX = (int) (Math.random() * width);
		int endY = (int) (Math.random() * height);
		while (endX == startX && endY == startY) {
			endX = (int) (Math.random() * width);
			endY = (int) (Math.random() * height);	
		}
		this.endCoordinate = new Coordinate(endX, endY);
	}
	
	
	
}
