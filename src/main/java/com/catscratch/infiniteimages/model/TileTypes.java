package com.catscratch.infiniteimages.model;

public class TileTypes {

	public static String LEFT_TOP_RIGHT_CLOSED = "0";
	public static String LEFT_BOTTOM_RIGHT_CLOSED = "1";
	public static String TOP_LEFT_BOTTOM_CLOSED = "2";
	public static String TOP_RIGHT_BOTTOM_CLOSED = "3";
	public static String TOP_BOTTOM_CLOSED = "4";
	public static String TOP_RIGHT_CLOSED = "5";
	public static String TOP_LEFT_CLOSED = "6";
	public static String LEFT_RIGHT_CLOSED = "7";
	public static String BOTTOM_RIGHT_CLOSED = "8";
	public static String BOTTOM_LEFT_CLOSED = "9";
	public static String TOP_CLOSED = "10";
	public static String LEFT_CLOSED = "11";
	public static String BOTTOM_CLOSED = "12";
	public static String RIGHT_CLOSED = "13";
	public static String NO_CLOSED = "14";
	
}
